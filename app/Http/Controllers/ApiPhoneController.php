<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Phone;
use Illuminate\Http\Request;

class ApiPhoneController extends Controller
{
    public function create($id)
    {
        $contact = Contact::find($id);

        return response()->json($contact);
    }

    public function destroy(Request $request,  $id)
    {
        $phone = Phone::find($id);
        try {
            $phone->delete();
            $request->session()->flash('status', __('Record successfully deleted'));
        } catch (\Exception $e) {
            $request->session()->flash('status', __('An error has occurred'));
        }

        return response()->json("Successfully completed!");
    }

    public function store(Request $request)
    {
        $contact = Contact::find($request->contact_id);
        $phoneData = $request->validate(
            [
                'number.*' => ['required', 'regex:/("+"|[0-9])[0-9]/'],
                'type.*' => 'required|min:3',
                'number_description.*' => 'nullable'
            ],
            [
                'number.*.required' => 'The number is required.',
                'number.*.regex' => 'The number can start with + sign, rest must be numbers.',
                'type.*.required' => 'The type is required.',
                'type.*.min' => 'The type must be at least of length 3',
            ]
        );

        for ($i = 0; $i < count($phoneData['number']); $i++) {
            $newPhone = new Phone();

            $newPhone->number = $phoneData['number'][$i];
            $newPhone->type = $phoneData['type'][$i];
            $newPhone->number_description = $phoneData['number_description'][$i];
            $newPhone->contact_id = $contact->id;

            $newPhone->save();
        }

        return redirect()->route('show.contact', $contact->id);
    }

    public function edit($id)
    {
        $phone = Phone::find($id);
        $contact = Contact::find($phone->contact()->first()->id);

        return response()->json("Successfully completed!");
    }

    public function update($id, Request $request)
    {
        $phoneData = $request->validate(
            [
                'number' => ['required', 'regex:/("+"|[0-9])[0-9]/'],
                'type' => 'required|min:3',
                'number_description' => 'nullable'
            ],
            [
                'number.required' => 'The number is required.',
                'number.regex' => 'The number can start with + sign, rest must be numbers.',
                'type.required' => 'The type is required.',
                'type.min' => 'The type must be at least of length 3',
            ]
        );

        $phone = Phone::find($id);
        $phone->update($phoneData);
        $phone->save();
        $contact_id = $request->contact_id;

        return response()->json("Successfully completed!");
    }

}

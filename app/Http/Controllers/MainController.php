<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use mysql_xdevapi\Exception;

class MainController extends Controller
{
    public function home()
    {
        $contacts = Contact::paginate(10);

        return view('home', compact('contacts'));
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $contactData = $request->validate([
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'city' => 'required',
            'image' => 'nullable|mimes:jpeg,bmp,png,jpg',
            'description' => 'nullable'
        ]);

        $contact = new Contact();
        $contact->fill($contactData);

        if ($request->image != null) {
            $imageName = $contact->first_name . '_' . $contact->last_name . '-image.' . $request->image->getClientOriginalExtension();
            $temp = $request->image->move(public_path('images'), $imageName);
            $test = explode("public\images", $temp);
            $fileLocation = "images" . $test[1];

            $contact->image = $fileLocation;
        }

        $phoneData = $request->validate(
            [
                'number.*' => ['required', 'regex:/("+"|[0-9])[0-9]/'],
                'type.*' => 'required|min:3',
                'number_description.*' => 'nullable'
            ],
            [
                'number.*.required' => 'The number is required.',
                'number.*.regex' => 'The number can start with + sign, rest must be numbers.',
                'type.*.required' => 'The type is required.',
                'type.*.min' => 'The type must be at least of length 3',
            ]
        );

        $contact->save();

        for ($i = 0; $i < count($phoneData['number']); $i++) {
            $newPhone = new Phone();

            $newPhone->number = $phoneData['number'][$i];
            $newPhone->type = $phoneData['type'][$i];
            $newPhone->number_description = $phoneData['number_description'][$i];
            $newPhone->contact_id = $contact->id;

            $newPhone->save();
        }

        return redirect()->route('home');
    }

    public function show($id)
    {
        $contact = Contact::find($id);
        $phones = $contact->phones()->get();

        return view('show', compact('contact', 'phones'));
    }

    public function edit($id)
    {
        $contact = Contact::find($id);

        return view('edit', compact('contact'));
    }

    public function update(Request $request, $id)
    {
        $contactData = $request->validate([
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'city' => 'required',
            'image' => 'nullable|mimes:jpeg,bmp,png,jpg',
            'description' => 'nullable'
        ]);

        $contact = Contact::find($id);

        if ($request->image != null) {
            $imageName = $contact->first_name . '_' . $contact->last_name . '-image.' . $request->image->getClientOriginalExtension();
            $temp = $request->image->move(public_path('images'), $imageName);
            $test = explode("public\images", $temp);
            $fileLocation = "images" . $test[1];

            $contact->image = $fileLocation;
        }

        $contact->update($contactData);
        $contact->save();

        return redirect()->route('show.contact', $contact->id);
    }


    public function destroy(Request $request, $id)
    {
        $contact = Contact::find($id);
        try {
            $contact->delete();
            $request->session()->flash('status', __('Record successfully deleted'));
        } catch (\Exception $e) {
            $request->session()->flash('status', __('An error has occurred'));
        }

        return redirect()->route('home');
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        try {
            $contacts = Contact::search($query)->paginate(10);
        } catch (\Exception $e) {
            $contacts = null;
        }


        return view('search', compact('contacts', 'query'));
    }
}

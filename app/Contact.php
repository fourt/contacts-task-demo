<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Contact extends Model
{
    use Searchable;

    protected $fillable = [
        'first_name',
        'last_name',
        'city',
        'description'
    ];

    public function phones()
    {
        return $this->hasMany(Phone::class, 'contact_id');
    }
}

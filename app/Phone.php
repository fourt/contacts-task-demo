<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Phone extends Model
{
    protected $fillable = [
        'number',
        'type',
        'number_description',
        'contact_id'
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }
}

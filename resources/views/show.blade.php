@extends('layout.app')
@section('content')

    <div class="home-row row">
        <div class="col-md-8" style="margin:0 auto">
            <div class="contact-data">
                <img src="{{asset($contact->image)}}" alt="contact image" class="contact-img">
                <div class="contact-info">
                    <h3>Name: {{$contact->first_name . ' ' . $contact->last_name}}</h3>
                    <h4>City: {{$contact->city}}</h4>
                </div>
            </div>
            <p style="margin-top: 1rem">Description: {{$contact->description}}</p>
            <a href="{{route('create.phone', $contact->id)}}" class="btn btn-primary btn-sm" style="margin-bottom: 1rem">Add Phone</a>
            <a href="{{route('edit.contact', $contact->id)}}" class="btn btn-primary btn-sm" style="margin-bottom: 1rem">Edit Contact</a>
            @if(count($phones)>0)
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Number</th>
                        <th scope="col">Number Description</th>
                        <th scope="col">Type</th>
                        <th scope="col">Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($phones as $phone)
                        <tr>
                            <td>{{$phone->number}}</td>
                            <td>{{$phone->number_description}}</td>
                            <td>{{$phone->type}}</td>
                            <td>
                                <a href="{{route('edit.phone', $phone->id)}}" class="btn btn-sm btn-primary">Edit</a>
                                <a data-method="delete" data-token="{{ csrf_token() }}"
                                   data-confirm="{{ __('Are you sure?') }}"
                                   href="{{ route('delete.phone', $phone->id) }}"
                                   class="btn btn-sm btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@endsection

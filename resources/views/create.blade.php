@extends('layout.app')
@section('content')
    <div class="create-body">
        <div class="col-lg-7 shadow-lg">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="margin-bottom: 0;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('store.contact')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="form-row">
                        <div class="col">
                            <label for="first_name">First Name</label> <span class="required">*</span>
                            <input type="text" id="first_name" name="first_name" class="form-control"
                                   placeholder="First Name" value="{{old('first_name')}}">
                        </div>
                        <div class="col">
                            <label for="last_name">Last Name</label> <span class="required">*</span>
                            <input type="text" id="last_name" name="last_name" class="form-control"
                                   placeholder="Last Name" value="{{old('last_name')}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control-file" name="image" id="image">
                </div>
                <div class="form-group">
                    <label for="city">City</label> <span class="required">*</span>
                    <input type="text" class="form-control" id="city" name="city"
                           placeholder="Enter City" value="{{old('city')}}">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    {!! Form::textarea('description', null, ['id' => 'description', 'rows' => 4, 'style' => 'resize:none', 'class' => 'form-control', 'value' => old('description')]) !!}
                </div>
                <div class="form-group" id="numberGroup">
                    <div class="form-row">
                        <div class="col">
                            <label for="number">Number</label> <span class="required">*</span>
                            <input type="text" id="number" name="number[]" class="form-control" placeholder="Number">
                            <small id="numberHelp" class="form-text text-muted">Number can start with + sign, rest must be
                                numbers (applies for all numbers).</small>
                        </div>
                        <div class="col">
                            <label for="number_type">Number Type</label> <span class="required">*</span>
                            <input type="text" id="number_type" name="type[]" class="form-control"
                                   placeholder="Number Type">
                        </div>
                        <div class="col-md-12">
                            <label for="number_description" style="margin-top: 1rem;">Phone Number Description</label>
                            <textarea id="number_description" rows="3" style="resize:none" class="form-control"
                                      name="number_description[]" cols="50" spellcheck="false"
                                      placeholder="Insert number description here..."></textarea>
                        </div>
                        <button id="addNumber" class="btn btn-info">Add another number</button>
                    </div>
                </div>
                <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $("#addNumber").click(function (e) {
                e.preventDefault();
                $("#submitBtn").before("<div class='form-group'><label>Extra Number</label><div class='form-row'><div class='col'>" +
                    "<input type='text' id='number' name='number[]' class='form-control' placeholder='Number'></div>" +
                    "<div class='col'><input type='text' name='type[]' class='form-control' placeholder='Number Type'></div>" +
                    "<div class='col-md-12'><textarea id='number_description' rows='3' placeholder='Insert number description here...' style='resize:none;margin-top:1rem;' class='form-control' name='number_description[]' cols='50' spellcheck='false'>" +
                    "</textarea></div><button type='button' class='removeNumberBtn btn btn-danger'>Remove number</button></div></div>");
            });
        });
    </script>
    <script>
        $(function () {
            $(document).on('click', ".removeNumberBtn", function () {
                $(this).parent().parent().remove();
            });
        });
    </script>
@endsection

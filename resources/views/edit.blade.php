@extends('layout.app')
@section('content')
    <div class="create-body">
        <div class="col-lg-7 shadow-lg">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="margin-bottom: 0;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h3 style="text-align: center">Edit {{$contact->first_name . ' ' . $contact->last_name}} info</h3>
            <form action="{{route('update.contact', $contact->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                {{ method_field('PUT') }}
                <div class="form-group">
                    <div class="form-row">
                        <div class="col">
                            <label for="first_name">First Name</label> <span class="required">*</span>
                            <input type="text" id="first_name" name="first_name" class="form-control"
                                   placeholder="First Name" value="{{$contact->first_name}}">
                        </div>
                        <div class="col">
                            <label for="last_name">Last Name</label> <span class="required">*</span>
                            <input type="text" id="last_name" name="last_name" class="form-control"
                                   placeholder="Last Name" value="{{$contact->first_name}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control-file" value="{{$contact->image}}" name="image" id="image">
                </div>
                <div class="form-group">
                    <label for="city">City</label> <span class="required">*</span>
                    <input type="text" class="form-control" id="city" name="city"
                           placeholder="Enter City" value="{{$contact->city}}">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    {!! Form::textarea('description', $contact->description, ['id' => 'description', 'rows' => 4, 'style' => 'resize:none', 'class' => 'form-control', 'value' => old('description')]) !!}
                </div>
                <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

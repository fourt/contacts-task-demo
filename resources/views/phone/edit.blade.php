@extends('layout.app')
@section('content')
    <div class="create-body">
        <div class="col-lg-7 shadow-lg">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="margin-bottom: 0;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h3 style="text-align: center">Edit number for {{$contact->first_name . ' ' . $contact->last_name}}</h3>
            <form action="{{route('update.phone', $phone->id)}}" method="POST">
                {{ method_field('PUT') }}
                @csrf
                <input type="hidden" name="contact_id" value="{{$contact->id}}">
                <div class="form-group" id="numberGroup">
                    <div class="form-row">
                        <div class="col">
                            <label for="number">Number</label> <span class="required">*</span>
                            <input type="text" id="number" name="number" value="{{$phone->number}}" class="form-control" placeholder="Number">
                            <small id="numberHelp" class="form-text text-muted">Number can start with + sign, rest must be
                                numbers (applies for all numbers).</small>
                        </div>
                        <div class="col">
                            <label for="number_type">Number Type</label> <span class="required">*</span>
                            <input type="text" id="number_type" value="{{$phone->type}}" name="type" class="form-control"
                                   placeholder="Number Type">
                        </div>
                        <div class="col-md-12">
                            <label for="number_description" style="margin-top: 1rem;">Phone Number Description</label>
                            <textarea id="number_description" rows="3" style="resize:none" class="form-control"
                                      name="number_description" cols="50" spellcheck="false"
                                      placeholder="Insert number description here...">{{$phone->number_description}}</textarea>
                        </div>
                    </div>
                </div>
                <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

@extends('layout.app')
@section('content')
    <div class="create-body">
        <div class="col-lg-7 shadow-lg">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="margin-bottom: 0;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h3 style="text-align: center">Add numbers for {{$contact->first_name . ' ' . $contact->last_name}}</h3>
            <form action="{{route('store.phone')}}" method="POST">
                @csrf
                <input type="hidden" name="contact_id" value="{{$contact->id}}">
                <div class="form-group" id="numberGroup">
                    <div class="form-row">
                        <div class="col">
                            <label for="number">Number</label> <span class="required">*</span>
                            <input type="text" id="number" name="number[]" class="form-control" placeholder="Number">
                            <small id="numberHelp" class="form-text text-muted">Number can start with + sign, rest must be
                                numbers (applies for all numbers).</small>
                        </div>
                        <div class="col">
                            <label for="number_type">Number Type</label> <span class="required">*</span>
                            <input type="text" id="number_type" name="type[]" class="form-control"
                                   placeholder="Number Type">
                        </div>
                        <div class="col-md-12">
                            <label for="number_description" style="margin-top: 1rem;">Phone Number Description</label>
                            <textarea id="number_description" rows="3" style="resize:none" class="form-control"
                                      name="number_description[]" cols="50" spellcheck="false"
                                      placeholder="Insert number description here..."></textarea>
                        </div>
                        <button id="addNumber" class="btn btn-info">Add another number</button>
                    </div>
                </div>
                <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $("#addNumber").click(function (e) {
                e.preventDefault();
                $("#submitBtn").before("<div class='form-group'><label>Extra Number</label><div class='form-row'><div class='col'>" +
                    "<input type='text' id='number' name='number[]' class='form-control' placeholder='Number'></div>" +
                    "<div class='col'><input type='text' name='type[]' class='form-control' placeholder='Number Type'></div>" +
                    "<div class='col-md-12'><textarea id='number_description' rows='3' placeholder='Insert number description here...' style='resize:none;margin-top:1rem;' class='form-control' name='number_description[]' cols='50' spellcheck='false'>" +
                    "</textarea></div><button type='button' class='removeNumberBtn btn btn-danger'>Remove number</button></div></div>");
            });
        });
    </script>
    <script>
        $(function () {
            $(document).on('click', ".removeNumberBtn", function () {
                $(this).parent().parent().remove();
            });
        });
    </script>
@endsection

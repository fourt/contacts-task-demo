@extends('layout.app')
@section('content')

    <div class="home-row row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">City</th>
                    <th scope="col">Numbers</th>
                    <th scope="col">Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <td>{{$contact->first_name}}</td>
                        <td>{{$contact->last_name}}</td>
                        <td>{{$contact->city}}</td>
                        <td>
                            @foreach($contact->phones()->get() as $phone)
                                @if($loop->last)
                                    {{$phone->number}}
                                @else
                                    {{$phone->number . ', '}}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <a href="{{route('show.contact', $contact->id)}}" class="btn btn-sm btn-primary">View</a>
                            <a href="{{route('edit.contact', $contact->id)}}" class="btn btn-sm btn-info">Edit</a>
                            <a data-method="delete" data-token="{{ csrf_token() }}"
                               data-confirm="{{ __('Are you sure?') }}"
                               href="{{ route('delete.contact', $contact->id) }}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$contacts->links()}}
        </div>
    </div>

@endsection

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'ApiController@home')->name('home');
Route::post('/contact/store', 'ApiController@store')->name('store.contact');
Route::get('/contact/{id}', 'ApiController@show')->name('show.contact');
Route::delete('/contact/{id}', 'ApiController@destroy')->name('delete.contact');
Route::get('/contact/edit/{id}', 'ApiController@edit')->name('edit.contact');
Route::put('/contact/update/{id}', 'ApiController@update')->name('update.contact');
Route::get('search', 'ApiController@search')->name('search');

Route::get('/phone/create/{id}', 'ApiPhoneController@create')->name('create.phone');
Route::post('/phone/store', 'ApiPhoneController@store')->name('store.phone');
Route::delete('/phone/{id}', 'ApiPhoneController@destroy')->name('delete.phone');
Route::get('/phone/edit/{id}', 'ApiPhoneController@edit')->name('edit.phone');
Route::put('/phone/update/{id}', 'ApiPhoneController@update')->name('update.phone');

<?php

Route::get('/', 'MainController@home')->name('home');
Route::get('/contact/create', 'MainController@create')->name('create.contact');
Route::post('/contact/store', 'MainController@store')->name('store.contact');
Route::get('/contact/{id}', 'MainController@show')->name('show.contact');
Route::delete('/contact/{id}', 'MainController@destroy')->name('delete.contact');
Route::get('/contact/edit/{id}', 'MainController@edit')->name('edit.contact');
Route::put('/contact/update/{id}', 'MainController@update')->name('update.contact');
Route::get('search', 'MainController@search')->name('search');

Route::get('/phone/create/{id}', 'PhoneController@create')->name('create.phone');
Route::post('/phone/store', 'PhoneController@store')->name('store.phone');
Route::delete('/phone/{id}', 'PhoneController@destroy')->name('delete.phone');
Route::get('/phone/edit/{id}', 'PhoneController@edit')->name('edit.phone');
Route::put('/phone/update/{id}', 'PhoneController@update')->name('update.phone');


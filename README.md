# Contacts Task - demo

## Prerequisites
* PHP >= 7.1.3
* Composer (https://getcomposer.org/)
* Web server such as XAMPP or WAMPP
* Laravel (To install use command: 'composer global require laravel/installer' after installing Composer)

## Instal steps
1. Set up an empty local mysql database for the project
2. Clone the repository
3. Enter the project folder
4. Create a folder called "images" inside the "public" folder
5. Copy the ".env.example" file and rename it to ".env". Inside it set up the database name, username and password
6. Run 'php artisan key:generate'
7. Run 'composer install'
8. Run 'php artisan storage:link'
9. For seeding the database with dummy data use 'php artisan db:seed'
10. To start the app run 'php artisan serve' and go to the link

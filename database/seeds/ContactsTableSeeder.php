<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Phone::class, 50)->create();
    }
}

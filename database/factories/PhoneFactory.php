<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Phone;
use Faker\Generator as Faker;

$factory->define(Phone::class, function (Faker $faker) {
    return [
        'number' => $faker->bankAccountNumber,
        'type' => $faker->word,
        'number_description' => $faker->text,
        'contact_id' => function () {
            return factory(App\Contact::class)->create()->id;
        }
    ];
});
